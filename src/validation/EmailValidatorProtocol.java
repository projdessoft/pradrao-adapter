package validation;

/**
 * Interface Target
 */
public interface EmailValidatorProtocol {
    boolean isEmail(String email);
}
