package validation;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Classe Adapter
 */
public class EmailValidatorAdapter implements EmailValidatorProtocol {
    @Override
    public boolean isEmail(String email) {
        try {
            // Classe Adaptee
            InternetAddress.parse(email);
            return true;
        } catch (AddressException e) {
            return false;
        }
    }
}
